package new_shop.model.entity;

import lombok.Data;

@Data
public class Product {

    private Integer id;
    private String name;

    public Product(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

}
