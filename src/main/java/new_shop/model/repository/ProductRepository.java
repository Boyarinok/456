package new_shop.model.repository;

import new_shop.model.entity.Product;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class ProductRepository {

    List<Product> products= Arrays.asList(new Product(1,"product_1"),
            new Product(2, "product_2"),
            new Product(3, "product_3"));

    public List<Product> findAll(){
        return this.products;
    }

}
